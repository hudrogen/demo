package com.stepic;

import java.util.Arrays;

import static com.stepic.Direction.DOWN;
import static com.stepic.Direction.LEFT;
import static com.stepic.Direction.RIGHT;
import static com.stepic.Direction.UP;

public class EightLevel {
  public static void main(String[] args) {
    final int M = 5;  //кол-во строк
    final int N = 8;  //кол-во столбцов
    process(M, N);
  }
  
  private static void process(int M, int N) {
    int[][] matrix = new int[M][N];
    int posX = 0;
    int posY = 0;
    Direction dir = RIGHT;
    int inc = 1;
    matrix[posX][posY] = inc;
    for (inc = 2; inc <= M * N;) {

      if (dir == RIGHT && (posX + 1) != N && matrix[posY][posX + 1] == 0) {
        posX++;
        matrix[posY][posX] = inc;
        inc++;
      } else {
        dir = DOWN;
      }
      
      if (dir == DOWN && (posY + 1) != M && matrix[posY + 1][posX] == 0) {
        posY++;
        matrix[posY][posX] = inc;
        inc++;
      } else {
        dir = LEFT;
      }
      
      if (dir == LEFT && (posX - 1) != -1 && matrix[posY][posX - 1] == 0) {
        posX--;
        matrix[posY][posX] = inc;
        inc++;
      } else {
        dir = UP;
      }
      
      if(dir == UP && (posY - 1) != -1 && matrix[posY - 1][posX] == 0) {
        posY--;
        matrix[posY][posX] = inc;
        inc++;
      } else {
        dir = RIGHT;
      }
    }

    for (int[] ints : matrix) {
      System.out.println(Arrays.toString(ints) + '\n');
    }
  }
}

enum Direction {
  RIGHT,
  DOWN,
  LEFT,
  UP
}
