package com.stepic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class NineLevel {
  public static void main(String[] args) {
    int N = 3;//строки
    int M = 5;//столбцы
    run(N, M);
  }
  
  private static void run(int N, int M) {
    int[][] rawArray = createRandom2DemArray(N, M);
    System.out.println("Сырой массив: ");
    print2dArray(rawArray);
    System.out.println();
    process(rawArray);
    System.out.println("Обработанный массив: ");
    print2dArray(rawArray);
  }
  private static int[][] createRandom2DemArray(int N, int M) {
    int[][] array = new int[N][M];
    Random rand = new Random();
    for (int i = 0; i < N; i++) {
      for (int j = 0; j < M; j++) {
        array[i][j] = rand.nextInt(11) - 5;  //диапазон от -5 до 5
      }
    }
    return array;
  }
  
  private static void print2dArray(int[][] array) {
    for (int[] ints : array) {
      System.out.println(Arrays.toString(ints));
    }
  }
  
  private static void process(int[][] src) {
    List<Integer> summList = new ArrayList();
    for (int i = 0; i < src[0].length; i++) {
      int summ = 0;
      for (int j = 0; j < src.length; j++) {
        summ += src[j][i];
      }
      summList.add(i, summ);
    }
    int minIndex = summList.indexOf(Collections.min(summList));
    int maxIndex = summList.indexOf(Collections.max(summList));
    
    //logic here
    int[] temp = new int[src.length];
    for (int i = 0; i < src.length; i++) {
      temp[i] = src[i][minIndex];
      src[i][minIndex] = src[i][maxIndex];
      src[i][maxIndex] = temp[i];
    }
  }
}
