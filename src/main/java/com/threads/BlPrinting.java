package com.threads;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BlPrinting {
  
  public Queue<LabelForPrint> labelsForPrintPack= new ConcurrentLinkedQueue<>();
  
  private int bufferSize = 2;
  
  public void collectLabelsAndPrint(LabelForPrint label) {
    label.setCreateTime(System.currentTimeMillis());
    addToCollection(label);
    if (labelsForPrintPack.size() == bufferSize) {
      printLabelPack();
    }
  }
  
  private synchronized boolean addToCollection(LabelForPrint label) {
    if (labelsForPrintPack.size() < bufferSize) {
      labelsForPrintPack.add(label);
      return true;
    }
    return false;
  }
  
  private synchronized void printLabelPack() {
    //тут проверка на то, как давно вызывался метод добавления в коллекция и проверка заполненности коллекции
    System.out.println("print label pack..." + labelsForPrintPack);
    labelsForPrintPack.clear();
  }
  
}
//время добавления в очередь
//отдельный метод на добавление в коллекцию и отдельный метод на печать
//если метод добавлени в коллекцию давно не вызвался то запускаем метод печати, обнуляем коллекцию
//как вариант проверять изменился ли размер коллекции через определенное время, если не изменился и не равно 0, то запускаем процесс печати
