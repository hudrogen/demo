package com.threads;

import java.time.LocalTime;

public class LabelForPrint {
  private String plu;
  private long createTime;

  public String getPlu() {
    return plu;
  }

  public void setPlu(String plu) {
    this.plu = plu;
  }

  public long getCreateTime() {
    return createTime;
  }

  public void setCreateTime(long createTime) {
    this.createTime = createTime;
  }

  @Override public String toString() {
    return "LabelForPrint{" +
        "plu='" + plu + '\'' +
        ", createTime=" + createTime +
        '}';
  }
}
